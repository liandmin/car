// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/store'
import ElementUI from 'element-ui'
// import es6Promise from 'es6-promise'

// 全局样式引入
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/normalize.css'
import './assets/css/commonstyle/index.less'
import './assets/css/modify.css'
// require("es6Promise").polyfill()
// 全局视图引入
import './components/common'

Vue.use(ElementUI)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: { App }
})
