import Vue from 'vue'
import Side from './sideBar.vue'
import Top from './topBar.vue'
import ErrorPage from './errorPage.vue'
Vue.component('Side', Side)
Vue.component('Top', Top)
Vue.component('ErrorPage', ErrorPage)
