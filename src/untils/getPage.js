/**
 * @para count : Number 内容总数
 * @para current : Number 希望展示的当前页
 * @para size : Number 每一页内容数
 * @return: Object pagination对象
 */

export default (count, current, size) => {
  count = parseInt(count)
  current = parseInt(current)
  size = (typeof size !== 'undefined') ? parseInt(size) : 10
  if (Number.isNaN(size) || size <= 0) {
    size = 10
  }
  if (Number.isNaN(count) || count < 0 || Number.isNaN(current)) {
    console.warn('请传入数字')
    return false
  }
  let i
  let pages = []
  let pageCount = Math.ceil(count / size)
  // if (current < 1 || pageCount > 0 && current > pageCount) {
  //   console.log('页数超过范围')
  //   return false
  // }
  if (pageCount > 0) {
    // 上一页和第2页相差2页以上时，用...占位
    if (current - 1 > 2 + 2) {
      pages.push(1, 2, -1)
    } else {
      for (i = 1; i < current - 1; i++) {
        pages.push(i)
      }
    }
  }
  return {
    count: count,
    current: current,
    size: size
  }
}
