import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import topBar from '@/components/common/topBar'
import sideBar from '@/components/common/sideBar'
import changePass from '@/components/common/changePass'
import Middle from '@/components/middle'
import User from '@/components/user'
import Page from '@/components/common/pagination'
import newList from '@/components/newList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/top',
      name: 'topBar',
      component: topBar
    },
    {
      path: '/side',
      name: 'sideBar',
      component: sideBar
    },
    {
      path: '/change',
      name: 'changePass',
      component: changePass
    },
    {
      path: '/date',
      name: 'middle',
      component: Middle
    },
    {
      path: '/page',
      name: 'page',
      component: Page
    },
    {
      path: '/user',
      name: 'user',
      component: User
    },
    {
      path: '/newList',
      name: 'newList',
      component: newList
    }
  ]
})
