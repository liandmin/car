# car
vue全家桶+elementUI

npm install

npm run dev

用户账户密码为123456
### 计划
1. 首页，根据不用身份侧边栏不同，展示不同内容
2. 顶部通栏做成组件

### 有待完善
1. 账号密码字体改大，提交按钮间距等分
2. IE9及以下显示提示页面，下载更高级浏览器
3. vuex共享数据，根据用户不同展示不同侧边栏

### 问题及方案
1. axios读取本地json,在dev.server配置
2. elementUI关于时间选择那块，月份、时间都不许需要带前置0
